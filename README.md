<h1>Bukkit/Spigot用プラグイン</h1>

ログイン　ログアウト　チャット時 コマンド実行時に音を鳴らすことができる

![ダウンロード](https://gitlab.com/kameme1208/kamesound/issues)

|config説明|  |
|-----|-----|
|JoinSound|ログイン時の音の設定|
|QuitSound|ログアウト時の音の設定|
|ChatSound|チャット時の音の設定|
|CmdsSound|コマンド実行時の音の設定|
<br>

|それぞれの説明|  |
|-----|-----|
|enable|有効無効の切り替え|
|allplayer|サーバー全体の人に鳴らすかその人だけに鳴らすか|
|sound|鳴らす音(コマンドのplaysoundで鳴らす形式の文字)|
|pitch|音の高さ(0.5~2まで)|

<br><br>

<h1>コマンド/設定説明</h1>
<h3>コマンド説明</h3>
/kamesound が追加されます (短縮系 /s /sound)

|コマンド|説明|
|-----|-----|
|/kamesound reload | Configをリロードします|
|/kamesound | ゲーム内でのそれぞれの音の設定GUIを開きます|



<h3>GUI説明</h3>
/kamesound (/s /sound)をコマンドで実行するとそれぞれのON/OFFが設定できるGUIが開きます<br>
![](https://gitlab.com/kameme1208/picture/uploads/eb6d3e72f43f670eb4c7e68fd915f006/1a94c98c5f6dd45f4a32b319585c08cf.gif)

レコードですべてのON/OFFができます。<br>
レッドストーントーチをクリック(左)することでレバーなり、チャット等の音がそのプレイヤーのみ再生されなくなります。<br>
レバーになったレッドストーントーチをクリックすることでトーチに戻り、音が再生されるようになります。

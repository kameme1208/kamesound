package kame.kamesound;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.io.Files;

public class Main extends JavaPlugin implements Listener {
	private static FileConfiguration config;
	private static final double version = 1.2;
	
	public void onEnable() {
		loadConfig();
		getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler(priority=EventPriority.MONITOR, ignoreCancelled = true)
	private void onCommandPreprocesst(PlayerCommandPreprocessEvent event) {
		playsound(config.getConfigurationSection("Sounds.CmdsSound"), event.getPlayer());
	}

	@EventHandler(priority=EventPriority.MONITOR, ignoreCancelled = true)
	private void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
		playsound(config.getConfigurationSection("Sounds.ChatSound"), event.getPlayer());
	}

	@EventHandler(priority=EventPriority.MONITOR)
	private void onPlayerLogin(PlayerJoinEvent event) {
		playsound(config.getConfigurationSection("Sounds.JoinSound"), event.getPlayer());
	}

	@EventHandler(priority=EventPriority.MONITOR)
	private void onPlayerLogout(PlayerQuitEvent event) {
		playsound(config.getConfigurationSection("Sounds.QuitSound"), event.getPlayer());
	}
	
	private void playsound(ConfigurationSection path, Player player) {
		if(!path.getBoolean("enable"))return;
		if(!path.getBoolean("allplayer")) {
			player.playSound(player.getLocation(), path.getString("sound"), 10000.0F, (float) path.getDouble("pitch"));
		}else {
			for(Player pl: getServer().getOnlinePlayers()) {
				pl.playSound(	 pl.getLocation(), path.getString("sound"), 10000.0F, (float) path.getDouble("pitch"));
			}
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		switch(cmd.getName()) {
		case "kamesound":
			if(args.length > 1) {
				if(args[0].equals("reload")) {
					loadConfig();
					sender.sendMessage(ChatColor.GREEN + "[kame.] config reloaded!");
					return true;
				}
			}else {
				sender.sendMessage(ChatColor.RED + "[kame.] unknown command");
				sender.sendMessage(ChatColor.RED + "[kame.] kamesound [reload]");
				return false;
			}
		}
		return true;
	}

	private void loadConfig() {
		this.saveDefaultConfig();
		config = getConfig();
		if(config.getDouble("version") != version)try {
			File file = new File(getDataFolder(), "config.yml");
			Files.move(file, new File(getDataFolder(), "config-old.yml"));
			saveDefaultConfig();
			config = YamlConfiguration.loadConfiguration(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length == 1) return starts(args[0], "reload");
		return new ArrayList<>();
	}

	private List<String> starts(String arg, String... args) {
		List<String> list = new ArrayList<>();
		for(String line : args)if(line.startsWith(arg))list.add(line);
		return list;
	}
}